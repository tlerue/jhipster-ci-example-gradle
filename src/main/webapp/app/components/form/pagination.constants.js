(function() {
    'use strict';

    angular
        .module('hipsterGitlabCiSampleApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
